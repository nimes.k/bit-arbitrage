var jwt = require('jsonwebtoken');

function generateToken(user) {
// sign with RSA SHA256
    var privateKey = "SuperSecretKey123*#";
    var token = jwt.sign(user, privateKey);

    return token;
}

function verifyToken(token) {
    return jwt.verify(token, 'SuperSecretKey123*#', function(err, decoded) {
        if(err)
            return {error: err};
        else
            return decoded;
    });
}

module.exports = {
    generateToken,
    verifyToken
};