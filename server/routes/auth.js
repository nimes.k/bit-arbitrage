var express = require('express');
var router = express.Router();
var {generateToken} = require("../utils/token");

/* GET home page. */
router.post('/', function(req, res, next) {
    console.log(req.body);
    if(req.body.email === "admin@rts.com.np" && req.body.password === "admin") {
        const {email} = req.body.email;

        var token = generateToken({email});

        res.send({
            status: 200,
            user: req.body.email,
            token
        });
    }
    else {
        res.send({
            error: "Invalid Credentials"
        });
    }
});

module.exports = router;
