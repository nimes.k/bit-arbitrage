var {verifyToken} = require("../utils/token");

function authenticate(req, res, next) {
    try {
        var token = req.headers["authorization"].split("bearer")[1];
        var decoded = verifyToken(token);
        req.token = decoded;

        if (decoded.error) {
            res.send({
                error: "Invalid Token!"
            });
        } else {
            next();
        }
    } catch (e) {
        res.send({
            error: "Invalid Token!"
        });
    }
}

module.exports = {
    authenticate
};